function covertToNumbers(str){
    let negativeNumber = false;
    let numberString = ''
    if(str.charAt(0)=='-'){
        negativeNumber=true;
        str = str.substring(2);
    }
    else{
        str = str.substring(1);
    }
    for(let digit in str){
        if((str[digit] >= '0' && str[digit] <= '9') || str[digit] == '.'){
            numberString = numberString + (str[digit]);
        }
        else if(str[digit] != ','){
            return 0;
        }
    }
    num = parseFloat(numberString);
    if(negativeNumber){
        num *= -1;
    }
    return num;
}

module.exports = covertToNumbers;