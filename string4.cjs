function convertToTitleCase(name){
    let TitleCased = '';
    for(let key in name){
        TitleCased += (name[key].toLowerCase())[0].toUpperCase() + (name[key].toLowerCase()).substring(1) + " ";
    }
    return TitleCased;
}

module.exports = convertToTitleCase;
