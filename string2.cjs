function ipAddressToArray(ipAddress){
    let isValidIp = true;
    let ipArray = ipAddress.split('.').reduce((numArray,part)=>{
        if(isValidIp){
        let numPart = Number(part);
        if(numPart >=0 && numPart<=256)
            numArray.push(numPart)
        else{
            isValidIp = false;
            numArray = [];
        }
        }
        return numArray
    },[])
    return ipArray;
}

module.exports = ipAddressToArray;