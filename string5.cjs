function convertToSentence(strArray){
    if(strArray.length===0){
        return [];
    }
    let sentence = strArray.reduce((sentence, word) => {
        return sentence + word +" ";
    },"")
    sentence = sentence.trim()+".";
    return sentence;
}

module.exports = convertToSentence;

